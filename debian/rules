#!/usr/bin/make -f
# -*- makefile -*-
# debian/rules for the Praat

DEBDIR = $(CURDIR)/debian
INSTDIR = $(DEBDIR)/praat

PIXDIR = usr/share/pixmaps
SVGDIR = usr/share/icons/hicolor/scalable/apps
BINDIR = usr/bin
APPDIR = usr/share/applications

SUBSTCMD = sed 's:@BINDIR@:$(BINDIR):g;s:@PIXDIR@:$(PIXDIR):g;	\
                s:@SVGDIR@:$(SVGDIR):g;s:@APPDIR@:$(APPDIR):g'

include /usr/share/dpkg/architecture.mk
-include /usr/share/dpkg/buildtools.mk
PKG_CONFIG ?= pkg-config

ifneq (,$(filter $(DEB_HOST_ARCH), i386))
    export DEB_CXXFLAGS_MAINT_APPEND=-ffloat-store
endif

%:
	dh $@

clean:
	dh clean
	# Fix the lacking cleaning command for the main/ directory
	[ ! -f makefile.defs ] || make -C main clean

execute_before_dh_auto_clean:
	touch makefile.defs

override_dh_auto_configure:
	# Generate the debhelper files
	for i in dirs install ; do 				\
		$(SUBSTCMD) < $(DEBDIR)/$$i.in > $(DEBDIR)/praat.$$i ;	\
	done
	# Get rid of the
	# dpkg-shlibdeps: warning: dependency on lib??? could be avoided if "debian/praat/usr/bin/praat" were not uselessly linked against it (they use none of its symbols).
	# warnings and useless dependencies
	cp makefiles/makefile.defs.linux.pulse-gcc makefile.defs
	# Add hardening flags
	cat debian/mk-vars-hardening >> makefile.defs

%.1: $(DEBDIR)/%.md
	# Generate the man page from the Markdown source
	pandoc --standalone --to man $< -o $@

override_dh_auto_build: praat.1 praat_nogui.1 praat-launch.1
	dh_auto_build
	cp makefiles/makefile.defs.linux.nogui makefile.defs
	# Add hardening flags
	cat debian/mk-vars-hardening >> makefile.defs
	rm -f melder/*.o sys/*.o fon/SoundRecorder.o fon/praat_Sound.o	\
		foned/FunctionEditor.o external/portaudio/*.o
	dh_auto_build

override_dh_auto_install:
	# Build and install the upstream changelog
	sed -n '/^R"~~~(/,/^)~~~"/{/^R"~~~(/!{/^)~~~"/!p}}' fon/manual_whatsnew.cpp > debian/what-is-new
	dh_installchangelogs debian/what-is-new
	dh_auto_install -- INSTALL=

ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
override_dh_auto_test:
	PRAAT=$(CURDIR)/praat_nogui debian/tests/run-tests
endif
