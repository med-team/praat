% PRAAT(1) | User Commands
%
% April 22, 2013

# NAME

praat - doing phonetics by compute

# SYNOPSIS

**praat**

# DESCRIPTION

This manual page documents briefly the **praat** command.

This manual page was written for the Debian distribution because the
original program does not have a manual page.

**praat** is a program for speech analysis and synthesis.
Several speech analysis functionalities are available: spectrograms,
cochleograms, and pitch and formant extraction. Articulatory
synthesis, as well as synthesis from pitch, formant, and intensity are
also available. Other features are segmentation, labelling using the
phonetic alphabet, and computation of statistics.
      
# SEE ALSO

**praat** has an on-line documentation; just launch **praat** and look
at the Help menu entry.

For further information, see the Praat website (http://www.praat.org).
A good Beginner's Guide, written by Sidney Wood, is available at
https://person2.sol.lu.se/SidneyWood/praate/frames.html

# AUTHOR

This manual page was written by Rafael Laboissière
<rafael@laboissiere.net> for the Debian system (but may be used by
others). Permission is granted to copy, distribute and/or modify this
document under the terms of the &gnu; General Public License, Version
3 any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL-3.
