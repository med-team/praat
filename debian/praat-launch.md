% PRAAT-LAUNCH(1) | User Commands
%
% 2023-12-04

# NAME

praat-launch - Wrapper for launching Praat with sound files or scripts as argument

# SYNOPSIS

**praat-launch** \[_file_]

# DESCRIPTION

The **praat-launch** command allows one to open sound files or scripts
with **praat** from the command line. The sound file can be of any
format accepted by Praat (for instance WAV, FLAC, or MP3). Scripts are
text files, usually with the .praat extension.

If no argument is ginve, then a new instance of praat is started.

A existent instance of the Praat program is used for opening the file.
It there is no instance running, a new one is created.

This program is intended to be used with the desktop file present in
the Debian distribution, but may be useful to regular users.

# AUTHOR

Rafael Laboissière <rafael@laboissiere.net>

# SEE ALSO

For further information, see the Praat website <http://www.praat.org>.
